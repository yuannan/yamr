# Yet Another Mirror Ranker

#### The fastest mirror ranker!⁺

![](docs/assets/updates_too_long.jpg)

![](docs/assets/updates_wojak.jpg)

![](docs/assets/updates_peter.jpg)

## Installation
Install via AUR helper such as `yay`
```bash
yay -S yamr
yamr -e
```

Manual install from AUR
```bash
git clone https://aur.archlinux.org/yamr.git
cd yamr
makepkg -si
yamr -e
```

## Usage
Run for an automatic update.
```bash
yamr
```

Help with all arguments:
```bash
yamr -h
```

### Config
`/etc/yamr.conf`: this file **ONLY** applies to the automatic systemd timer and is **NOT** a global config file. The contents of this file are loaded as raw arguments line by line. This means there cannot be *anything* else within this config file.

## Dependencies
||credits|
|---|---|
|[argparse](https://github.com/p-ranav/argparse)|Argument parser.|
|[curl](https://github.com/curl/curl)|The main backend for downloads.|
|[maxmind](https://github.com/maxmind)|For automatic geolocation.|
|[spdlog](https://github.com/gabime/spdlog)|Logging library.|
|you?|Make a PR...|

## Building
##### Download method #0
```bash
git clone --recursive git@gitlab.com:yuannan/yamr.git
```
##### Download method #1
```bash
git clone git@gitlab.com:yuannan/yamr.git
git submodule init
git submodule update
```

#### Install Dependencies
```bash
pacman -Syu geoip
```

##### Build method #0
```bash
./clean_build.sh
```

##### Build method #1
```bash
mkdir build && cd build
cmake .. && cmake --build . -j
./yamr -h
```

⁺or your money back!
