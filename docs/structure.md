# yamr program structure

```
📂 .
├── 📗 clean_build.sh
├── 📓 CMakeLists.txt
├── 📂 docs
│   └── 📕 structure.md (you are here)
├── 📂 Include
│   ├── 📂 argparse
│   ├── 📗 cmake.hpp.in
│   ├── 📓 CMakeLists.txt
│   ├── 📂 spdlog
│   ├── 📗 yamr.hpp
│   └── 📗 yamr_tests.hpp
├── 📔 LICENSE
├── 📕 README.md
└── 📂 src
    └── 📗 yamr.cpp

```
