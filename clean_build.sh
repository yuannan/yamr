build_dir="build"
rm -rf $build_dir
mkdir -p $build_dir
cd $build_dir
cmake .. && cmake --build . -j
./yamr -h && ./yamr -o /tmp/yamr.mirrorlist
