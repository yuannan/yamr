#ifndef YAMR_H
#define YAMR_H

// program config
#define PROGRAM_NAME "yamr"
#include "include/cmake.hpp"
#define PROGRAM_DESC "The fastest mirror ranker!⁺"
#define PROGRAM_EPI "⁺or your money back!"

// libs
#include <curl/curl.h>
#include <fstream>
#include <unistd.h>
#include <filesystem>
#include <set>
#include <omp.h>
#include <thread>

// spdlog config
#include "spdlog/spdlog.h"
#define INIT_SPDLOG_DEFAULT_LOGGING_LEVEL 2

// argparse config
#include "argparse/argparse.hpp"

// systemd config
#define SYSTEMCTL_EXE_PATH "/bin/systemctl"
#define SYSTEMCTL_TIMER_NAME "yamr.timer"
#define SYSTEMCTL_ENABLE_TIMER SYSTEMCTL_EXE_PATH " enable " SYSTEMCTL_TIMER_NAME " && echo DONE"
#define SYSTEMCTL_DISABLE_TIMER SYSTEMCTL_EXE_PATH " disable " SYSTEMCTL_TIMER_NAME " && echo DONE"

/* simple IP address providers
#define IP_ADDRESS_URL "https://ident.me"
#define IP_ADDRESS_URL "https://ipecho.net/plain"
#define IP_ADDRESS_URL "https://api.ipify.org"
*/
#define IP_ADDRESS_URL "https://ifconfig.me"

// get config
#define URL_ARCH_KEY "$arch"
#define URL_ARCH_KEY_DEFAULT "x86_64"
#define URL_REPO_KEY "$repo"
#define URL_REPO_KEY_DEFAULT "core"
#define URL_COUNTRY_PARM "?country="
#define URL_COUNTRY_KEY "$country"
#define URL_COUNTRY_KEY_DEFAULT "all"
#define URL_PROTOCOL_PARM "&protocol="
#define URL_PROTOCOL_KEY "$protocol"
#define URL_PROTOCOL_KEY_DEFAULT "https"
#define URL_IP_PARM "&ip_version="
#define URL_IP_KEY "$ip"
#define URL_IP_KEY_DEFAULT '4'
#define URL_MIRROR_STATUS_PARM "&use_mirror_status="
#define URL_MIRROR_STATUS_KEY "$mirror_status"
#define URL_MIRROR_STATUS_KEY_DEFAULT "on"
#define MIRROR_FETCH_URL_LINUX_ARCH "https://archlinux.org/mirrorlist/" URL_COUNTRY_PARM URL_COUNTRY_KEY URL_PROTOCOL_PARM URL_PROTOCOL_KEY
// rank config
#define RANK_TOTAL_TIMEOUT_DEFAULT 10
#define RANK_TIMEOUT_HOST "localhost"
#define RANK_REPO_SIZE_CUTOFF 100
#define SPDLOG_CURL_INFO_OUT "{} {:.2f}MB {:.2f}MB/s"
#define SPDLOG_CURL_DEBUG_OUT "Failed " SPDLOG_CURL_INFO_OUT
// write config
#define OUTPUT_MIRROR_COUNT_DEFAULT 5
#define OUTPUT_MIRROR_PATH_DEFAULT "/etc/pacman.d/mirrorlist"

// data config
argparse::ArgumentParser program(PROGRAM_NAME, VERSION_PROJECT);
const char* log_level_strings[] = {"trace", "debug", "info", "warn", "error", "critical", "off"};
std::map<std::string, std::string> country_hmap = {
	{"ALL","All Countries (Global)"},
	{"AR","Argentina"},
	{"AU","Australia"},
	{"AT","Austria"},
	{"AZ","Azerbaijan"},
	{"BD","Bangladesh"},
	{"BY","Belarus"},
	{"BE","Belgium"},
	{"BA","Bosnia and Herzegovina"},
	{"BR","Brazil"},
	{"BG","Bulgaria"},
	{"KH","Cambodia"},
	{"CA","Canada"},
	{"CL","Chile"},
	{"CN","China"},
	{"CO","Colombia"},
	{"HR","Croatia"},
	{"CZ","Czechia"},
	{"DK","Denmark"},
	{"EC","Ecuador"},
	{"EE","Estonia"},
	{"FI","Finland"},
	{"FR","France"},
	{"GE","Georgia"},
	{"DE","Germany"},
	{"GR","Greece"},
	{"HK","Hong Kong"},
	{"HU","Hungary"},
	{"IS","Iceland"},
	{"IN","India"},
	{"ID","Indonesia"},
	{"IR","Iran"},
	{"IE","Ireland"},
	{"IL","Israel"},
	{"IT","Italy"},
	{"JP","Japan"},
	{"KZ","Kazakhstan"},
	{"KE","Kenya"},
	{"LV","Latvia"},
	{"LT","Lithuania"},
	{"LU","Luxembourg"},
	{"MU","Mauritius"},
	{"MX","Mexico"},
	{"MD","Moldova"},
	{"MC","Monaco"},
	{"NL","Netherlands"},
	{"NC","New Caledonia"},
	{"NZ","New Zealand"},
	{"MK","North Macedonia"},
	{"NO","Norway"},
	{"PY","Paraguay"},
	{"PL","Poland"},
	{"PT","Portugal"},
	{"RO","Romania"},
	{"RU","Russia"},
	{"RE","Réunion"},
	{"RS","Serbia"},
	{"SG","Singapore"},
	{"SK","Slovakia"},
	{"SI","Slovenia"},
	{"ZA","South Africa"},
	{"KR","South Korea"},
	{"ES","Spain"},
	{"SE","Sweden"},
	{"CH","Switzerland"},
	{"TW","Taiwan"},
	{"TH","Thailand"},
	{"TR","Turkey"},
	{"UA","Ukraine"},
	{"GB","United Kingdom"},
	{"US","United States"},
	{"UZ","Uzbekistan"},
	{"VN","Vietnam"}
};
#define TEST_REPOS 3
#define TEST_REPO_INDEX_DEFAULT 1
std::string test_repo_strings[TEST_REPOS] = {"multilib", "core", "extra"};
enum test_repo_enum {multilib, core, extra, tre_size};
#define TEST_REPO_EXTS 2
#define TEST_REPO_EXT_DEFAULT 0
std::string test_ext_strings[] = {"db", "files"};
enum test_ext_enum {db, files, tee_size};
typedef struct {
	// flow control vars
	bool invalid_args;
	bool quiet_exit;
	bool require_root;
	// data storage vars
	std::set<std::string> get_country_codes;
	// get vars 
	bool disable_encryption;
	char ip_v;
	bool mirror_status;
	std::string ip;
	// rank vars
	int test_threads;
	int test_timeout;
	test_repo_enum test_repo;
	test_ext_enum test_ext;
	// write vars
	int mirrors;
	bool output_header;
	std::string output_path;
	bool console_out;
} config;
config g_config;

#include "yamr_tests.hpp"
#endif
