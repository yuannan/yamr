#ifndef YAMR_SPDLOG_TESTS
#define YAMR_SPDLOG_TESTS

#include "yamr.hpp"

// spdlog functions
void spdlog_test_output(void){
	spdlog::trace("Starting {}", __func__);
	spdlog::critical("{}", spdlog::get_level());
	spdlog::trace("Trace Output");
	spdlog::debug("Debug Output");
	spdlog::info("Info Output");
	spdlog::warn("Warn Output");
	spdlog::error("Error Output");
	spdlog::critical("Critical Output\n");
}

void spdlog_test_level_sweep(void){
	spdlog::trace("Starting {}", __func__);
	spdlog_test_output();
	spdlog::set_level(spdlog::level::trace);
	spdlog_test_output();
	spdlog::set_level(spdlog::level::debug);
	spdlog_test_output();
	spdlog::set_level(spdlog::level::info);
	spdlog_test_output();
	spdlog::set_level(spdlog::level::warn);
	spdlog_test_output();
	spdlog::set_level(spdlog::level::err);
	spdlog_test_output();
	spdlog::set_level(spdlog::level::critical);
	spdlog_test_output();
	spdlog::set_level(spdlog::level::off);
	spdlog_test_output();
	spdlog::set_level(spdlog::level::trace);
}

void spdlog_test_set_level_sweep(void){
	spdlog::trace("Starting {}", __func__);
	spdlog::set_level(spdlog::level::trace);
	const int sweeps = 10;
	for(int i = 0; i < sweeps; i++){
		spdlog_test_output();
	}
}

// printing functions
void print_mirrors(std::vector<std::string> mirrors){
	spdlog::trace("Starting {}", __func__);
	spdlog::info("mirror vector size: {}", mirrors.size());
	for(int m = 0; m < (int) mirrors.size(); m++){
		spdlog::info("mirror URL: {} {}", m, mirrors[m].c_str());
	}
}

void print_pairs(int pair_count, std::pair<curl_off_t, std::string> print_pair[]){
	spdlog::trace("Starting {}", __func__);
	for(int p = 0; p < pair_count; p++){
		spdlog::info("Speed: {} URL: {}", print_pair[p].first, print_pair[p].second.c_str());
	}
}

void print_country_codes(void){
	for (const auto& c : country_hmap){
		spdlog::info("{}\t{}", c.first, c.second);
	}
}

// TEST STATIC VARS
// test repo vars
static_assert(TEST_REPOS == tre_size);
static_assert(TEST_REPO_INDEX_DEFAULT < TEST_REPOS);
// test repo ext vars
static_assert(TEST_REPO_EXTS == tee_size);
static_assert(TEST_REPO_EXT_DEFAULT < TEST_REPO_EXTS);

#endif
