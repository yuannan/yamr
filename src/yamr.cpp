#include "yamr.hpp"

// call back functions
static size_t write_cb_stub(char *data, size_t n, size_t l, void *userp) {
	//spdlog::trace("Starting {}", __func__);
	(void) data;
	(void) userp;
	return n*l;
}

size_t write_cb_string(void *contents, size_t size, size_t nmemb, std::string *s) {
	spdlog::trace("Starting {}", __func__);
	size_t newLength = size*nmemb;
	try {
		s->append((char*)contents, newLength);
		spdlog::debug("{} string:\n{}", __func__, s->c_str());
	} catch (std::bad_alloc &e) {
		spdlog::error("{} bad alloc error", __func__);
		newLength = 0;
	}

	return newLength;
}

// helper functions
void string_replace_first( std::string& s, std::string const& toReplace, std::string const& replaceWith) {
	spdlog::trace("Starting {}", __func__);
	std::size_t pos = s.find(toReplace);
	if (pos != std::string::npos) { 
		s.replace(pos, toReplace.length(), replaceWith);
		spdlog::debug("{} replacing '{}' with '{}'", __func__, toReplace, replaceWith);
	}
}

void curl_to_string(std::string hit_url, std::string& str) {
	spdlog::trace("Starting {}", __func__);
	CURL *curl;
	CURLcode res;
	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, hit_url.c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_cb_string);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &str);

		/* Perform the request, res will get the return code */
		res = curl_easy_perform(curl);
		/* Check for errors */
		if (res != CURLE_OK) {
			spdlog::error("{} failed: {}", __func__, curl_easy_strerror(res));
		}

		/* always cleanup */
		curl_easy_cleanup(curl);
	}
}

void download_mirror_server_URLs(std::string mirror_server_URLs[]) {
	spdlog::trace("Starting {}", __func__);
	// create base URL
	std::string AL_base_url = MIRROR_FETCH_URL_LINUX_ARCH;

	// protocol
	string_replace_first(AL_base_url, URL_PROTOCOL_KEY, (g_config.disable_encryption)? "http" : "https");
	if (g_config.ip_v == 'b') {
		AL_base_url = AL_base_url + URL_IP_PARM + '4' + URL_IP_PARM + '6';
		spdlog::debug("Both IP versions are being used");
	} else {
		AL_base_url = AL_base_url + URL_IP_PARM + g_config.ip_v;
		spdlog::debug("IP version: {}", g_config.ip_v);
	}

	// mirror status
	if (g_config.mirror_status) {
		AL_base_url = AL_base_url + URL_MIRROR_STATUS_PARM + "on";
		spdlog::debug("Mirror Status: On");
	} else {
		spdlog::debug("Mirror Status: Off");
	}

	// country insertion
	spdlog::debug("Base URL: {}", AL_base_url);
	int c = 0;
	for (std::string m : g_config.get_country_codes) {
		std::string AL_mirror_fetch_source = AL_base_url;
		string_replace_first(AL_mirror_fetch_source, URL_COUNTRY_KEY, m);
		spdlog::debug("{} end URL: {}", __func__, AL_mirror_fetch_source);
		mirror_server_URLs[c] = AL_mirror_fetch_source;
		c++;
	}
}

void download_URLs_to_string(std::string country_mirror_URLs[], std::vector<std::string>& country_mirror_string) {
	spdlog::trace("Starting {}", __func__);
	int country_count = g_config.get_country_codes.size();
	for (int m = 0; m < country_count; m++) {
		std::string s;
		curl_to_string(country_mirror_URLs[m].c_str(), s);
		spdlog::debug("{} string:\n{}", __func__, s);
		country_mirror_string.push_back(s);
	}
}

void extract_string_URLs(std::vector<std::string> input_server_string, std::vector<std::string>& clean_mirrors) {
	spdlog::trace("Starting {}", __func__);

	std::set<std::string> mirror_set;
	for (int s = 0; s < (int) input_server_string.size(); s++) {
		// process per country servers
		std::istringstream iss(input_server_string[s]);
		for (std::string line; std::getline(iss, line);) {
			std::size_t pos = line.find("#Server = ");
			if (pos != std::string::npos) {
				// extract URL
				std::string repo_url = line.substr(10).c_str();
				spdlog::debug("{} repo_url: {}", __func__, repo_url);
				mirror_set.insert(repo_url);
			}
		}
	}

	// change set back into vector
	for(auto m : mirror_set) {
		spdlog::debug("Set Output: {}", m);
		clean_mirrors.push_back(m);
	}
}

void get_curl_dl_speed(const char* spd_hit_url, curl_off_t& spd) {
	spdlog::trace("Starting {}", __func__);
	// init curl session
	CURL* curl_handle = curl_easy_init();
	curl_easy_setopt(curl_handle, CURLOPT_URL, spd_hit_url);
	curl_easy_setopt(curl_handle, CURLOPT_TIMEOUT, g_config.test_timeout);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_cb_stub);
	// check results
	CURLcode res = curl_easy_perform(curl_handle);
	if(res == CURLE_OK) {
		// get average download speed
		curl_off_t curl_speed_val;
		res = curl_easy_getinfo(curl_handle, CURLINFO_SPEED_DOWNLOAD_T, &curl_speed_val);
		if (res == CURLE_OK) {
			// speed okay
			curl_off_t curl_size_downloaded_val;
			res = curl_easy_getinfo(curl_handle, CURLINFO_SIZE_DOWNLOAD_T, &curl_size_downloaded_val);
			if (res == CURLE_OK) {
				// size okay
				if (curl_size_downloaded_val > RANK_REPO_SIZE_CUTOFF){
					// over the proper size for files, aka none empty repo
					if (curl_speed_val > 0) {
						spd = curl_speed_val;
						spdlog::info(SPDLOG_CURL_INFO_OUT, spd_hit_url, (float) curl_size_downloaded_val/1000.0/1000.0, (float) spd/1000.0/1000.0);
					}
				} else {
					// file is under the file size cut off, aka empty repo
					spd = 0;
					spdlog::debug(SPDLOG_CURL_DEBUG_OUT, spd_hit_url, curl_size_downloaded_val,(float) spd/1000.0/1000.0);
				}
			} else {
				spdlog::warn("Error in getting curl download size.");
			}
		} else {
			spdlog::warn("Error in getting curl download speed.");
		}
	} else {
		spdlog::warn("Error while fetching '{}' : {}", spd_hit_url, curl_easy_strerror(res));
		spd = 0;
	}

	spdlog::debug("{} {}B/s", spd_hit_url, spd);

	/* cleanup curl stuff */
	curl_easy_cleanup(curl_handle);
}

std::string exec(const char* cmd) {
	spdlog::trace("Starting {}", __func__);
    char buffer[128];
    std::string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw std::runtime_error("popen() failed!");
    try {
        while (fgets(buffer, sizeof buffer, pipe) != NULL) {
            result += buffer;
        }
    } catch (...) {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}

void automatic_country_set(void){
	spdlog::trace("Starting {}", __func__);

	// clear country code
	g_config.get_country_codes.clear();

	// find ip and geo locate it
	curl_to_string(IP_ADDRESS_URL, g_config.ip);
	spdlog::debug("IP: {}", g_config.ip);
	std::string geoip_cmd = "geoiplookup " + g_config.ip;
	std::string geoip_cmd_raw_return = exec(geoip_cmd.c_str());
	std::string geoip_cmd_extracted = geoip_cmd_raw_return.substr(23);
	spdlog::debug("Raw Extracted GeoIP Country: {}", geoip_cmd_extracted);
	if (geoip_cmd_extracted.find(',') == 2) {
		// comma in 2th char probably a legit code
		std::string geoip_2C = geoip_cmd_extracted.substr(0, 2);
		if (country_hmap.count(geoip_2C)) {
			// found 2 char country in database
			spdlog::info("Automatic GeoIP Country: {} -> {}", geoip_2C, country_hmap[geoip_2C]);
			g_config.get_country_codes.insert(geoip_2C);
		} else {
			spdlog::error("Automatic GeoIP found unsupported country: {} {} {}", g_config.ip, geoip_2C, geoip_cmd_extracted);
		}
	} else {
		spdlog::error("Automatic GeoIP pattern match failed: {} {}", g_config.ip, geoip_cmd_extracted);
		g_config.get_country_codes.insert("all");
	}
}

// main functions
void get_mirrors(std::vector<std::string>& clean_mirrors) {
	spdlog::trace("Starting {}", __func__);
	// download mirror server URLs
	int country_count = g_config.get_country_codes.size();
	if (country_count == 0){
		g_config.get_country_codes.insert("all");
		country_count = g_config.get_country_codes.size();
	}
	std::string mirror_fetch_urls[country_count];
	download_mirror_server_URLs(mirror_fetch_urls);

	// download per country mirrors to string
	std::vector<std::string> fetched_mirror_servers;
	download_URLs_to_string(mirror_fetch_urls, fetched_mirror_servers);

	// process strings to extract URLs
	extract_string_URLs(fetched_mirror_servers, clean_mirrors);
}

void rank_mirrors(std::vector<std::string>& clean_mirrors_to_rank, std::pair<curl_off_t, std::string> ranked_pairs[]) {
	spdlog::trace("Starting {}", __func__);
	// rank the mirrors by speed

	// create array with hit speeds
	int mirror_count = (int) clean_mirrors_to_rank.size();
	omp_set_num_threads(g_config.test_threads);
	#pragma omp parallel
	{
		#pragma omp for nowait
		for (int m = 0; m < mirror_count; m++) {
			// make hit url
			std::string mirror_url = clean_mirrors_to_rank[m];
			// replace raw URLs with hit URL
			std::string hit_repo = test_repo_strings[g_config.test_repo];
			string_replace_first(mirror_url, URL_REPO_KEY, hit_repo);
			string_replace_first(mirror_url, URL_ARCH_KEY, URL_ARCH_KEY_DEFAULT);
			std::string hit_ext = test_ext_strings[g_config.test_ext];
			std::string hit_file = hit_repo + "." + hit_ext;
			std::string hit_url = mirror_url + '/' + hit_file;
			const char* hit_url_c = hit_url.c_str();

			curl_off_t curl_speed_val;
			get_curl_dl_speed(hit_url_c, curl_speed_val);
			if( curl_speed_val != 0){
				ranked_pairs[m] = std::make_pair(curl_speed_val, clean_mirrors_to_rank[m]);
			} else {
				ranked_pairs[m] = std::make_pair(curl_speed_val, RANK_TIMEOUT_HOST);
			}
		}
	}

	// sort the newly initiated array
	std::sort(ranked_pairs, ranked_pairs + mirror_count);
}

void write_mirrors(int total_mirrors, std::pair<curl_off_t, std::string> mirror_pairs[]) {
	spdlog::trace("Starting {}", __func__);
	// write out the ranked mirrors to a file or stdout

	// out header
	std::time_t t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	std::stringstream b8601;
	b8601 << std::put_time( std::localtime( &t ), "%FT%T%z" );
	std::string b8601_str =  b8601.str();
	std::string out_header = "# yamr " + b8601_str + "\n";
	// out body
	int last_m = std::max(0, (total_mirrors - g_config.mirrors));
	std::string out_body;
	for (int m = total_mirrors-1; m >= last_m; m--) {
		if (mirror_pairs[m].second != RANK_TIMEOUT_HOST) {
			out_body += "Server = " + mirror_pairs[m].second + "\n";
		}
	}

	// concat the strings
	std::string out_str;
	if (g_config.output_header) {
		out_str = out_header + out_body;
	} else {
		out_str = out_body;
	}

	// write to file stream
	if (g_config.console_out) {
		std::cout << out_str;
	} else {
		std::fstream outfile;
		outfile.open(g_config.output_path, std::ios_base::out);
		if (!outfile.is_open()) {
			spdlog::error("{} error at {}", __func__, g_config.output_path);
		} else {
			outfile.write(out_str.data(), out_str.size());
			spdlog::debug("Written new mirrors: {}", g_config.output_path);
		}
	}
}

// init functions
void init_spdlog(void){
	// set default log level and logging format
	spdlog::set_level((spdlog::level::level_enum) INIT_SPDLOG_DEFAULT_LOGGING_LEVEL);
	spdlog::set_pattern("%^[%L]%$ [%C-%m-%d %H:%M:%S] [%t] %v ");
}

void init_argparse_logging(void){
	spdlog::trace("Starting {}", __func__);
	// logging args
	program.add_argument("-l", "--log-level")
			.help("Log level = [0-6] -> [trace, debug, info, warn, error, critical, off]")
			.scan<'i', int>();

	program.add_argument("-q", "--quiet")
			.help("Quiet = log off")
			.implicit_value(true);

	program.add_argument("-V", "--verbose")
			.help("Verbose = log trace")
			.implicit_value(true);
}

void init_argparse_extra(void) {
	spdlog::trace("Starting {}", __func__);
	// systemd enables
	program.add_argument("-e", "--enable")
			.help("Enable = enables systemd timer for automatic updates")
			.implicit_value(true);

	program.add_argument("-d", "--disable")
			.help("Disable = disables systemd timer for automatic updates")
			.implicit_value(true);
}

void init_argparse_get(void){
	spdlog::trace("Starting {}", __func__);
	// get args
	program.add_argument("-c", "--countries")
			.help("Countries = input country codes")
			.nargs(argparse::nargs_pattern::any);
	
	program.add_argument("-lc", "--list-countries")
			.help("List Countries: lists supported country codes")
			.implicit_value(true);
	
	program.add_argument("-ip", "--ip-version")
			.help("Ip Version = [4, 6, b] -> [ipv4, ipv6, both]");

	program.add_argument("-ne", "--no-encryption")
			.help("No Encryption = disables HTTPS encryption")
			.implicit_value(true);
	
	program.add_argument("-nms", "--no-mirror-status")
			.help("No Mirror Status = disables repo status")
			.implicit_value(true);
}

void init_argparse_rank(void){
	spdlog::trace("Starting {}", __func__);
	// rank args
	program.add_argument("-t", "--threads")
			.help("Threads = number of threads to use")
			.scan<'i', int>();

	program.add_argument("-to", "--timeout")
			.help("Time Out = maximum total request time")
			.scan<'i', int>();
	
	program.add_argument("-tr", "--test-repo")
			.help("Test Repo = [0-2] -> [multilib, core, extra]")
			.scan<'i', int>();
	
	program.add_argument("-tx", "--test-ext")
			.help("Test Extension = [0-1] -> [db, files]")
			.scan<'i', int>();
}

void init_argparse_write(void){
	spdlog::trace("Starting {}", __func__);
	// write args
	program.add_argument("-o", "--output")
			.help("Output = file path");

	program.add_argument("-m", "--mirrors")
			.help("Mirrors = number of output repositories")
			.scan<'i', int>();

	program.add_argument("-cn", "--console")
			.help("Console = show output instead of writing to a file")
			.implicit_value(true);

	program.add_argument("-nh", "--no-output-header")
			.help("No Headers = raw output for scripts")
			.implicit_value(true);

}

void init_argparse_logging_process(void){
	spdlog::trace("Starting {}", __func__);
	// process args
	if (program.is_used("-q")) {
		spdlog::set_level((spdlog::level::level_enum) 6);
	}

	if (program.is_used("-V")) {
		spdlog::set_level((spdlog::level::level_enum) 0);
		spdlog::info("Log Level = 0 -> trace (verbose)");
	}

	if (program.is_used("-l")) {
		int argi_log_level = program.get<int>("-l");
		if(argi_log_level >= 0 && argi_log_level <= 6){
			spdlog::set_level((spdlog::level::level_enum) argi_log_level);
			spdlog::info("Log Level = {} -> {}", argi_log_level, log_level_strings[argi_log_level]);
		} else {
			spdlog::error("Log Level: out of bounds: {}", argi_log_level);
		}
	}
}

void init_argparse_extra_process(void) {
	if (program.is_used("-e") && program.is_used("-d")) {
		spdlog::error("Cannot enable and disable automatic updates at the same time!");
		g_config.invalid_args = true;
	} else {
		if (program.is_used("-e")) {
			// try and enable service
			if (access(SYSTEMCTL_EXE_PATH, X_OK)) {
				// cannot exec systemctl
				spdlog::error("Cannot exec systemctl. {}", SYSTEMCTL_EXE_PATH);
				g_config.quiet_exit = true;
			} else {
				spdlog::info("Enabling automatic updates");
			}
		}

		if (program.is_used("-d")) {
			// try and enable service
			if (access(SYSTEMCTL_EXE_PATH, X_OK)) {
				// cannot exec systemctl
				spdlog::error("Cannot exec systemctl. {}", SYSTEMCTL_EXE_PATH);
				g_config.quiet_exit = true;
			} else {
				spdlog::info("Disabling automatic updates");
			}
		}
	}
}

void init_argparse_get_process(void){
	spdlog::trace("Starting {}", __func__);
	// get args
	if (program.is_used("-c")) {
		std::vector<std::string> input_country_codes = program.get<std::vector<std::string>>("-c");
		for (int c = 0; c < input_country_codes.size(); c++){
			// convert country code to upper case
			std::string uc = input_country_codes[c];
			std::transform(uc.begin(), uc.end(), uc.begin(), [](unsigned char c){ return std::toupper(c); });

			spdlog::debug("{} {}", uc, uc.size());
			if (uc == "ALL" || uc == "AL") {
				g_config.get_country_codes.clear();
				g_config.get_country_codes.insert("all");
				break;
				spdlog::debug("All countries selected.");
			} else if (uc.size() == 2){
				// checks if the input is within supported codes
				if (country_hmap.count(uc)) {
					spdlog::info("{} -> {} queued", uc, country_hmap[uc]);
					g_config.get_country_codes.insert(uc);
				} else {
					spdlog::error("Country code not supported: {}", uc);
					g_config.invalid_args = true;
				}
			} else{
				spdlog::error("Country code *MUST* be 2 characters: {}", uc);
				g_config.invalid_args = true;
			}
		}
	} else {
		// automatic country detection
		if (!(g_config.quiet_exit || g_config.invalid_args || g_config.require_root)) {
			// assign country if there is no early exits
			automatic_country_set();
		}
	}

	if (program.is_used("-lc")) {
		print_country_codes();
		g_config.quiet_exit = true;
	}

	if (program.is_used("-ip")) {
		std::string ip_v_str = program.get<std::string>("-ip");
		if (ip_v_str.size() == 1) {
			char fc = ip_v_str[0];
			switch (fc) {
				case '4':
					spdlog::info("IP version set to: {}", 4);
					g_config.ip_v = fc;
					break;
				case '6':
					spdlog::info("IP version set to: {}", 6);
					g_config.ip_v = fc;
					break;
				case 'b':
				case 'B':
					spdlog::info("IP version set to: {}", "both");
					g_config.ip_v = 'b';
					break;
				default:
					spdlog::error("IP version invalid: {}", fc);
					break;
			}
		} else {
			// too large
			spdlog::error("IP Version arg too large: {}", ip_v_str);
			g_config.invalid_args = true;
		}
	} else {
		g_config.ip_v = URL_IP_KEY_DEFAULT;
	}

	if (program.is_used("-ne")) {
		spdlog::info("No Encryption: Sources will be HTTP only");
		g_config.disable_encryption = true;
	}

	if (program.is_used("-nms")) {
		spdlog::info("No Mirror Status");
		g_config.mirror_status = false;
	} else {
		g_config.mirror_status = true;
	}
}

void init_argparse_rank_process(void){
	spdlog::trace("Starting {}", __func__);
	// rank args
	if (program.is_used("-t")) {
		int test_threads = program.get<int>("-t");
		if (test_threads < 0) {
			// invalid threads
			spdlog::error("Invalid Threads (Negative): {}", test_threads);
			g_config.invalid_args = true;
		} else if (test_threads == 0) {
			g_config.test_threads = std::thread::hardware_concurrency();
			spdlog::info("Threads = {} (System Max)", g_config.test_threads);
		} else {
			spdlog::info("Threads = {}", test_threads);
			g_config.test_threads = test_threads;
		}
	} else {
		g_config.test_threads = std::thread::hardware_concurrency();
		spdlog::debug("Threads: {} (Auto System Max)", g_config.test_threads);
	}

	if (program.is_used("-to")) {
		int test_timeout = program.get<int>("-to");
		if (test_timeout < 0) {
			// invalid timeout
			spdlog::error("Invalid timeout: {}", test_timeout);
		} else {
			g_config.test_timeout = test_timeout;
			spdlog::info("Time Out = {}", test_timeout);
		}
	} else {
		g_config.test_timeout = RANK_TOTAL_TIMEOUT_DEFAULT;
	}

	if (program.is_used("-tr")) {
		int test_repo_i = program.get<int>("-tr");
		if (test_repo_i >= 0 && test_repo_i < test_repo_enum::tre_size) {
			// proper enum
			g_config.test_repo = (test_repo_enum) test_repo_i;
			spdlog::info("Test Repo = {} -> {}", g_config.test_repo, test_repo_strings[g_config.test_repo]);
		} else {
			g_config.test_repo = (test_repo_enum) TEST_REPO_INDEX_DEFAULT;
			spdlog::error("Test Repo out of bounds {}", test_repo_i);
			g_config.invalid_args = true;
		}
	} else {
		g_config.test_repo = (test_repo_enum) TEST_REPO_INDEX_DEFAULT;
	}

	if (program.is_used("-tx")) {
		int test_ext_i = program.get<int>("-tx");
		if (test_ext_i >= 0 && test_ext_i < test_ext_enum::tee_size) {
			// proper enum
			g_config.test_ext = (test_ext_enum) test_ext_i;
			spdlog::info("Test Ext = {} -> {}", g_config.test_ext, test_ext_strings[g_config.test_ext]);
		} else {
			g_config.test_ext = (test_ext_enum) TEST_REPO_EXT_DEFAULT;
			spdlog::error("Test Ext out of bounds {}", test_ext_i);
			g_config.invalid_args = true;
		}
	} else {
		g_config.test_ext = (test_ext_enum) TEST_REPO_EXT_DEFAULT;
	}
}

void init_argparse_write_process(void){
	spdlog::trace("Starting {}", __func__);
	// write args
	if (program.is_used("-o")) {
		g_config.output_path = program.get<std::string>("-o");
		spdlog::debug("Raw Output Path: {}", g_config.output_path);
		// check if file already exists
		try {
			if (access(g_config.output_path.c_str(), F_OK)) {
				// file doesn't exist
				// check if the parent dir is writable
				size_t p_dir_marker = g_config.output_path.find_last_of('/');
				std::string parent_dir = g_config.output_path.substr(0, p_dir_marker+1);
				if (access(parent_dir.c_str(), F_OK)){
					// parent dir does not exist
					spdlog::critical("Parent dir access denied: {}", parent_dir);
					g_config.invalid_args = true;
				} else {
					// check if parent dir is writable
					if (access(parent_dir.c_str(), W_OK)) {
						// parent dir is not writable
						spdlog::critical("Parent dir write denied: {}", parent_dir);
						g_config.invalid_args = true;
					} else {
						spdlog::info("Creating mirrorlist: {}", g_config.output_path);
					}
				}
			} else {
				// file exists
				// check if file is dir
				if (std::filesystem::is_directory(g_config.output_path.c_str())) {
					spdlog::critical("Dir detected: {}", g_config.output_path);
					g_config.invalid_args = true;
				// check if file is writable
				} else if (access(g_config.output_path.c_str(), W_OK)) {
					// no write access to file
					spdlog::critical("Write denied: {}", g_config.output_path);
					g_config.require_root = true;
				} else {
					spdlog::info("Output = {}", g_config.output_path);
				}
			}
		} catch (std::filesystem::filesystem_error& e) {
			spdlog::critical("Output Error {}", e.what());
			g_config.invalid_args = true;
		}
	} else {
		g_config.output_path = OUTPUT_MIRROR_PATH_DEFAULT;
		g_config.require_root = true;
	}

	if (program.is_used("-m")) {
		g_config.mirrors = program.get<int>("-m");
		spdlog::info("Mirrors = {}", g_config.mirrors);
	} else {
		g_config.mirrors = OUTPUT_MIRROR_COUNT_DEFAULT;
	}

	if (program.is_used("-nh")) {
		g_config.output_header = false;
		spdlog::info("Output header disabled ");
	} else {
		g_config.output_header = true;
	}

	if (program.is_used("-cn")) {
		g_config.console_out = true;
		g_config.require_root = false;
	}
}

bool is_root(void) {
	spdlog::trace("Starting {}", __func__);
	uid_t uid = getuid();
	uid_t euid = geteuid();
	spdlog::debug("UID:{} EUID:{}", uid, euid);

	return (uid == 0);
}

void arg_exits(void) {
	spdlog::trace("Starting {}", __func__);
	// premature exits
	if (g_config.quiet_exit) {
		spdlog::debug("Quiet exit");
		exit(0);
	}

	if (g_config.invalid_args) {
		spdlog::critical("Invalid Arguments!");
		exit(2);
	}

	if (g_config.require_root) {
		if (!is_root()){
			spdlog::critical("Root required");
			exit(1);
		} 
	}
}

void init_argparse_post(void){
	spdlog::trace("Starting {}", __func__);
	// systemd services
	if (program.is_used("-e")) {
		// enable service
		std::string enable_time_str = exec(SYSTEMCTL_ENABLE_TIMER);
		spdlog::debug("Enable Systemd Timer: {}", enable_time_str);
		if (enable_time_str == "DONE\n") {
			spdlog::info("Automatic updates enabled successfully");
		} else {
			spdlog::error("Failed to enable automatic updates");
		}
		g_config.quiet_exit = true;
	}

	if (program.is_used("-d")) {
		// disable service
		std::string disable_time_str = exec(SYSTEMCTL_DISABLE_TIMER);
		spdlog::debug("Disable Systemd Timer: {}", disable_time_str);
		if (disable_time_str == "DONE\n") {
			spdlog::info("Automatic updates disabled successfully");
		} else {
			spdlog::error("Failed to disable automatic updates");
		}
		g_config.quiet_exit = true;
	}
}

void init_argparse(int argc, char** argv){
	spdlog::trace("Starting {}", __func__);
	// parse input args
	program.add_description(PROGRAM_DESC);
	program.add_epilog(PROGRAM_EPI);

	init_argparse_logging();
	init_argparse_extra();
	init_argparse_get();
	init_argparse_rank();
	init_argparse_write();

	// parsing args
	try {
		program.parse_args(argc, argv);
	} catch (const std::runtime_error& err) {
		spdlog::critical("CRITICAL ERROR! {}", err.what());
		g_config.invalid_args = true;
	}
	arg_exits();

	init_argparse_logging_process();
	init_argparse_extra_process();
	init_argparse_get_process();
	init_argparse_rank_process();
	init_argparse_write_process();
	init_argparse_post();
	arg_exits();
}

time_t time_ms(void) {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (time_t) tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

// program entry point
int main(int argc, char** argv) {
	// init and parse args
	std::time_t t_start = time_ms();
	spdlog::debug("Start Time: {}", t_start);
	init_spdlog();
	try {
		init_argparse(argc, argv);
	} catch (std::logic_error& e) {
		spdlog::critical("Error in arg parse: {}", e.what());
		g_config.invalid_args = true;
	}
	curl_global_init(CURL_GLOBAL_DEFAULT);

	// create results vector for storing mirror URLs
	std::vector<std::string> clean_mirrors;
	get_mirrors(clean_mirrors);

	// create results pair
	int mirror_count = clean_mirrors.size();
	std::pair<curl_off_t, std::string> result_pairs[mirror_count];
	rank_mirrors(clean_mirrors, result_pairs);

	// write out results
	write_mirrors(mirror_count, result_pairs);
	std::time_t t_stop = time_ms();
	spdlog::debug("Stop Time: {}", t_stop);
	std::time_t t_delta = t_stop - t_start;
	spdlog::info("yamr wrote to {} in <{}ms", g_config.output_path, t_delta+1);

	return EXIT_SUCCESS;
}
